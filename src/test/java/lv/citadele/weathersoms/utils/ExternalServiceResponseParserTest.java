package lv.citadele.weathersoms.utils;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lv.citadele.weathersoms.entity.WeatherEntity;
import lv.citadele.weathersoms.enums.ExternalService;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class ExternalServiceResponseParserTest {

    @Test
    public void firstServiceTestSuccess() {
        JsonObject json = new JsonParser().parse("{" +
                "  \"main\": {" +
                "    \"temp\": 273.15," +
                "    \"pressure\": 1019," +
                "    \"humidity\": 98" +
                "  }," +
                "  \"name\": \"RIGA\"" +
                "}").getAsJsonObject();

        WeatherEntity weather = ExternalServiceResponseParser.getParsedFirstServiceWeatherEntity(json);
        Assert.assertEquals(98, weather.getHumidity());
        Assert.assertEquals(1019, weather.getPressure());
        Assert.assertEquals(BigDecimal.valueOf(0).setScale(2, BigDecimal.ROUND_HALF_UP), weather.getTemperature());
        Assert.assertEquals("RIGA", weather.getCity());
        Assert.assertEquals(ExternalService.OPEN_WEATHER_MAP, weather.getExternalService());
    }

    @Test
    public void secondServiceTestSuccess() {
        //some test :)
    }

    @Test(expected = RuntimeException.class)
    public void firstService_InvalidTemp() {
        JsonObject json = new JsonParser().parse("{" +
                "  \"main\": {" +
                "    \"temp\": fake," +
                "    \"pressure\": 1019," +
                "    \"humidity\": 98" +
                "  }," +
                "  \"name\": \"RIGA\"" +
                "}").getAsJsonObject();
       ExternalServiceResponseParser.getParsedFirstServiceWeatherEntity(json);
    }

    @Test(expected = RuntimeException.class)
    public void firstService_InvalidPressure() {
        JsonObject json = new JsonParser().parse("{" +
                "  \"main\": {" +
                "    \"temp\": 271.5," +
                "    \"pressure\": string," +
                "    \"humidity\": 98" +
                "  }," +
                "  \"name\": \"RIGA\"" +
                "}").getAsJsonObject();
        ExternalServiceResponseParser.getParsedFirstServiceWeatherEntity(json);
    }

    @Test(expected = RuntimeException.class)
    public void firstService_InvalidHumidity() {
        JsonObject json = new JsonParser().parse("{" +
                "  \"main\": {" +
                "    \"temp\": 271.5," +
                "    \"pressure\": 1019," +
                "    \"humidity\": string" +
                "  }," +
                "  \"name\": \"RIGA\"" +
                "}").getAsJsonObject();
        ExternalServiceResponseParser.getParsedFirstServiceWeatherEntity(json);
    }

    @Test(expected = RuntimeException.class)
    public void firstService_MissedTemp() {
        JsonObject json = new JsonParser().parse("{" +
                "  \"main\": {" +
                "    \"pressure\": 1019," +
                "    \"humidity\": 98" +
                "  }," +
                "  \"name\": \"RIGA\"" +
                "}").getAsJsonObject();
        ExternalServiceResponseParser.getParsedFirstServiceWeatherEntity(json);
    }


    @Test(expected = RuntimeException.class)
    public void firstService_MissedHumidity() {
        JsonObject json = new JsonParser().parse("{" +
                "  \"main\": {" +
                "    \"temp\": 271.5," +
                "    \"pressure\": 1019" +
                "  }," +
                "  \"name\": \"RIGA\"" +
                "}").getAsJsonObject();
        ExternalServiceResponseParser.getParsedFirstServiceWeatherEntity(json);
    }

    @Test(expected = RuntimeException.class)
    public void firstService_MissedPressure() {
        JsonObject json = new JsonParser().parse("{" +
                "  \"main\": {" +
                "    \"temp\": 271.5," +
                "    \"humidity\": 98" +
                "  }," +
                "  \"name\": \"RIGA\"" +
                "}").getAsJsonObject();
        ExternalServiceResponseParser.getParsedFirstServiceWeatherEntity(json);
    }
    @Test(expected = RuntimeException.class)
    public void firstService_MissedName() {
        JsonObject json = new JsonParser().parse("{" +
                "  \"main\": {" +
                "    \"temp\": 271.5," +
                "    \"pressure\": string," +
                "    \"humidity\": 98" +
                "  }" +
                "}").getAsJsonObject();
        ExternalServiceResponseParser.getParsedFirstServiceWeatherEntity(json);
    }


    @Test(expected = RuntimeException.class)
    public void firstService_MissedMain() {
        JsonObject json = new JsonParser().parse("{" +
                "  \"name\": \"RIGA\"" +
                "}").getAsJsonObject();
       ExternalServiceResponseParser.getParsedFirstServiceWeatherEntity(json);
    }
}