package lv.citadele.weathersoms.service;

import lv.citadele.weathersoms.dao.WeatherRepository;
import lv.citadele.weathersoms.entity.WeatherEntity;
import lv.citadele.weathersoms.enums.ExternalService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class WeatherServiceTest {
    public static final String RIGA = "Riga";
    @Mock
    private WeatherRepository weatherRepository;
    @Mock
    private ExternalServiceCaller externalServiceCaller;
    @InjectMocks
    private WeatherService weatherService;


    @Test(expected = RuntimeException.class)
    public void testWeather_NoDataFromAnyService() {
        when(weatherRepository.findByCity(RIGA)).thenReturn(Optional.empty());
        when(externalServiceCaller.callFirstService(RIGA)).thenThrow(new RuntimeException("Service unavailable"));
        when(externalServiceCaller.callSecondService(RIGA)).thenThrow(new RuntimeException("Service unavailable"));
        weatherService.getWeather(RIGA);
    }
    @Test()
    public void testWeather_FirstServiceMethodExecution() {
        WeatherEntity weatherEntity = getTestWeatherEntity();

        when(weatherRepository.findByCity(RIGA)).thenReturn(Optional.empty());
        when(externalServiceCaller.callFirstService(RIGA)).thenReturn(weatherEntity);
        when(weatherRepository.save(weatherEntity)).thenReturn(weatherEntity);

        weatherService.getWeather(RIGA);
        
        verify(externalServiceCaller).callFirstService(RIGA);
        verify(externalServiceCaller, never()).callSecondService(RIGA);
        verify(weatherRepository).save(weatherEntity);
    }

    @Test()
    public void testWeather_SecondServiceMethodExecution() {
        WeatherEntity weatherEntity = getTestWeatherEntity();

        when(weatherRepository.findByCity(RIGA)).thenReturn(Optional.empty());
        when(externalServiceCaller.callFirstService(RIGA)).thenThrow(new RuntimeException("Service unavailable"));
        when(externalServiceCaller.callSecondService(RIGA)).thenReturn(weatherEntity);
        when(weatherRepository.save(weatherEntity)).thenReturn(weatherEntity);

        weatherService.getWeather(RIGA);
        
        verify(externalServiceCaller).callFirstService(RIGA);
        verify(externalServiceCaller).callSecondService(RIGA);
        verify(weatherRepository).save(weatherEntity);
    }

    @Test()
    public void testWeather_NoNeedToCallServicesTakeWeatherFromDb() {
        WeatherEntity weatherEntity = getTestWeatherEntity();

        when(weatherRepository.findByCity(RIGA)).thenReturn(Optional.of(weatherEntity));

        weatherService.getWeather(RIGA);

        verify(externalServiceCaller, never()).callFirstService(RIGA);
        verify(externalServiceCaller, never()).callSecondService(RIGA);
        verify(weatherRepository, never()).save(weatherEntity);

    }

    @Test()
    public void testWeather_WeatherDataInDBDeprecated() {
        WeatherEntity weatherEntity = getTestWeatherEntity();
        weatherEntity.setLastUpdate(new Date(System.currentTimeMillis() - 600001));

        when(weatherRepository.findByCity(RIGA)).thenReturn(Optional.of(weatherEntity));
        when(externalServiceCaller.callFirstService(RIGA)).thenThrow(new RuntimeException("Service unavailable"));
        when(externalServiceCaller.callSecondService(RIGA)).thenReturn(weatherEntity);
        when(weatherRepository.save(weatherEntity)).thenReturn(weatherEntity);

        weatherService.getWeather(RIGA);

        verify(externalServiceCaller).callFirstService(RIGA);
        verify(externalServiceCaller).callSecondService(RIGA);
        verify(weatherRepository).save(weatherEntity);
    }

    private WeatherEntity getTestWeatherEntity() {
        WeatherEntity weatherEntity = new WeatherEntity();
        weatherEntity.setExternalService(ExternalService.OPEN_WEATHER_MAP);
        weatherEntity.setCity(RIGA);
        weatherEntity.setLastUpdate(new Date());
        weatherEntity.setHumidity(60);
        weatherEntity.setTemperature(BigDecimal.valueOf(-2.5));
        weatherEntity.setPressure(1000);
        return weatherEntity;
    }
}