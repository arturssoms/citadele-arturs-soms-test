package lv.citadele.weathersoms.dao;

import lv.citadele.weathersoms.entity.WeatherEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface WeatherRepository extends CrudRepository<WeatherEntity, Long> {
    Optional<WeatherEntity> findByCity(String city);
}
