package lv.citadele.weathersoms.dao;

import lv.citadele.weathersoms.entity.UserActivityEntity;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserActivityEntity, Long> {
}
