package lv.citadele.weathersoms.utils;

import com.google.gson.JsonObject;
import lv.citadele.weathersoms.entity.WeatherEntity;
import lv.citadele.weathersoms.enums.ExternalService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

public class ExternalServiceResponseParser {
    public static WeatherEntity getParsedFirstServiceWeatherEntity(JsonObject jsonObject) {
        if (!jsonObject.has("main")) {
            throw new RuntimeException("First service returned invalid \"main\" field. Json - " +  jsonObject.getAsString());
        }
        final JsonObject main = jsonObject.getAsJsonObject("main");
        if (!main.has("temp")) {
            throw new RuntimeException("First service returned invalid \"temp\" field. Json - " +  jsonObject.getAsString());
        }
        if (!main.has("humidity")) {
            throw new RuntimeException("First service returned invalid \"humidity\" field. Json - " +  jsonObject.getAsString());
        }
        if (!main.has("pressure")) {
            throw new RuntimeException("First service returned invalid \"pressure\" field. Json - " +  jsonObject.getAsString());
        }

        if (!jsonObject.has("name")) {
            throw new RuntimeException("First service returned invalid \"name\" field. Json - " +  jsonObject.getAsString());
        }

        BigDecimal temp = main.get("temp").getAsBigDecimal();
        int humidity = main.get("humidity").getAsInt();
        int pressure = main.get("pressure").getAsInt();
        String city = jsonObject.get("name").getAsString();

        WeatherEntity weatherEntity = new WeatherEntity();
        BigDecimal tempInCels = temp.subtract(new BigDecimal(273.15)).setScale(2, RoundingMode.HALF_UP);
        weatherEntity.setTemperature(tempInCels);
        weatherEntity.setHumidity(humidity);
        weatherEntity.setPressure(pressure);
        weatherEntity.setCity(city);
        weatherEntity.setLastUpdate(new Date());
        weatherEntity.setExternalService(ExternalService.OPEN_WEATHER_MAP);
        return weatherEntity;
    }

    public static WeatherEntity getParsedSecondServiceWeatherEntity(JsonObject jsonObject) {
        return getParsedFirstServiceWeatherEntity(jsonObject);
    }
}
