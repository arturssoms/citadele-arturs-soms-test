package lv.citadele.weathersoms.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "user_activity")
public class UserActivityEntity {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String city;

    @Column(nullable = false)
    private String ip;
}
