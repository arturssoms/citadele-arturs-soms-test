package lv.citadele.weathersoms.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lv.citadele.weathersoms.enums.ExternalService;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Audited
@Table(name = "weather", indexes = {
        @Index(columnList = "city", name = "weather_city_idx")
})
public class WeatherEntity {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String city;

    @Column(nullable = false, precision=3, scale=2)
    private BigDecimal temperature;

    @Column(nullable = false)
    private int humidity;

    @Column(nullable = false)
    private int pressure;

    @JsonIgnore
    @Column(nullable = false)
    private Date lastUpdate;

    @JsonIgnore
    @Column(nullable = false)
    private ExternalService externalService;
}
