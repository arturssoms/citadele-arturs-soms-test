package lv.citadele.weathersoms.enums;

public enum ExternalService {
    OPEN_WEATHER_MAP, ANOTHER_TEST_SERVICE;
}
