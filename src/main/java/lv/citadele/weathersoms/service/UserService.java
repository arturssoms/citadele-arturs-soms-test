package lv.citadele.weathersoms.service;

import lv.citadele.weathersoms.dao.UserRepository;
import lv.citadele.weathersoms.entity.UserActivityEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public void saveActivity(String city, String ip) {
        UserActivityEntity userActivity = new UserActivityEntity();
        userActivity.setCity(city);
        userActivity.setIp(ip);
        userRepository.save(userActivity);
    }
}
