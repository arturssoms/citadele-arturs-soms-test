package lv.citadele.weathersoms.service;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.InetAddress;

@Service
public class GeoLocationService {

    @Value("${geo.file.location}")
    private String geoFileLocation;

    public String getCity(String ip) {
        try {
            DatabaseReader dbReader = new DatabaseReader.Builder(new ClassPathResource(geoFileLocation).getInputStream()).build();
            InetAddress ipAddress = InetAddress.getByName(ip);
            CityResponse response = dbReader.city(ipAddress);
            return response.getCity().getName();
        } catch (GeoIp2Exception | IOException e) {
            throw new RuntimeException("Couldn't get city by ip - " + ip, e);
        }
    }
}
