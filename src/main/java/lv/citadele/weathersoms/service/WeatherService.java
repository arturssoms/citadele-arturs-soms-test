package lv.citadele.weathersoms.service;

import lv.citadele.weathersoms.dao.WeatherRepository;
import lv.citadele.weathersoms.entity.WeatherEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class WeatherService {
    private Logger logger = LoggerFactory.getLogger(WeatherService.class);


    @Value("${weather.deprecation.millis:600000}")
    private int weatherDeprecationMillis = 600000;

    @Autowired
    private WeatherRepository weatherRepository;

    @Autowired
    private ExternalServiceCaller externalServiceCaller;

    @Cacheable("weather")
    public WeatherEntity getWeather(String city) {
        logger.info("get weather for city - " + city);
        final Optional<WeatherEntity> weatherEntityFromDB = weatherRepository.findByCity(city);
        //take weather data from db in case data is already up to date.
        if (isWeatherUpToDate(weatherEntityFromDB)) {
            return weatherEntityFromDB.get();
        }

        WeatherEntity weatherEntity = getWeatherFromAvailableService(city);
        weatherEntityFromDB.ifPresent(weatherEntityDB -> weatherEntity.setId(weatherEntityDB.getId()));
        return weatherRepository.save(weatherEntity);
    }

    private WeatherEntity getWeatherFromAvailableService(String city) {
        try {
            return externalServiceCaller.callFirstService(city);
        } catch (RuntimeException e) {
            logger.error("First external service call error", e);
        }

        try {
            return externalServiceCaller.callSecondService(city);
        } catch (RuntimeException e) {
            logger.error("Second external service call error", e);
        }
        //etc
        throw new RuntimeException("No weather data available for city - " + city);
    }

    private boolean isWeatherUpToDate(Optional<WeatherEntity> weatherEntityOpt) {
        return weatherEntityOpt.isPresent() && weatherEntityOpt.get().getLastUpdate().getTime() > (System.currentTimeMillis() - weatherDeprecationMillis);
    }

}
