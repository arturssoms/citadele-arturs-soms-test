package lv.citadele.weathersoms.service;

import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import lv.citadele.weathersoms.entity.WeatherEntity;
import lv.citadele.weathersoms.utils.ExternalServiceResponseParser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@Service
public class ExternalServiceCaller {
    private Logger logger = LoggerFactory.getLogger(WeatherService.class);
    private final RestTemplate restTemplate;
    @Value("${geo.external.service1}")
    private String geoExternalService1;
    @Value("${geo.external.service2}")
    private String geoExternalService2;
    @Autowired
    public ExternalServiceCaller(RestTemplateBuilder builder) {
        restTemplate = builder
                .setConnectTimeout(Duration.ofMillis(500))
                .setReadTimeout(Duration.ofMillis(500))
                .build();
    }

    public String callService(String link) {
        return restTemplate.getForObject(link, String.class);
    }
    public WeatherEntity callFirstService(String city) {
        JsonObject json = getJsonDataFromExternalServer(city, geoExternalService1);
        return ExternalServiceResponseParser.getParsedFirstServiceWeatherEntity(json);
    }
    public WeatherEntity callSecondService(String city) {
        //should be used another service call.
        JsonObject json = getJsonDataFromExternalServer(city, geoExternalService2);
        return ExternalServiceResponseParser.getParsedSecondServiceWeatherEntity(json);
    }
    private JsonObject getJsonDataFromExternalServer(String city, String service) {
        String response = null;
        try {
            String replaceCityMock = service.replace("#CITY", city);
            logger.info("External service call - " + replaceCityMock);
            response = callService(replaceCityMock);
            if (StringUtils.isBlank(response)) {
                throw new RuntimeException("GEO external rest service response is blank. Url - " + service + " city - " + city);
            }
            return new JsonParser().parse(response).getAsJsonObject();
        } catch (RestClientException e) {
            throw new RuntimeException("GEO external rest service error. Url - " + service + " city - " + city, e);
        } catch (JsonParseException e) {
            throw new RuntimeException("GEO external service json parse error. Url - " + service + " city - " + city + " json - " + response, e);
        }
    }
}
