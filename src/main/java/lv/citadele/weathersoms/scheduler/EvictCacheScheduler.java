package lv.citadele.weathersoms.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class EvictCacheScheduler {
    private static final Logger log = LoggerFactory.getLogger(EvictCacheScheduler.class);

    @CacheEvict(value = "weather", allEntries = true)
    @Scheduled(fixedRate = 60000)
    public void reportCurrentTime() {
        log.info("weather cache evicted");
    }
}
