package lv.citadele.weathersoms.rest;

import lv.citadele.weathersoms.entity.WeatherEntity;
import lv.citadele.weathersoms.service.GeoLocationService;
import lv.citadele.weathersoms.service.UserService;
import lv.citadele.weathersoms.service.WeatherService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class WeatherController {

    @Autowired
    private GeoLocationService geoLocationService;

    @Autowired
    private WeatherService weatherService;

    @Autowired
    private UserService userService;


    @GetMapping("/weather")
    public WeatherEntity getWeather(HttpServletRequest request) {

        final String ip = getIP(request);
        //must be implemented by country as well.
        final String city = this.geoLocationService.getCity(ip);
        userService.saveActivity(city, ip);
        return weatherService.getWeather(city);
    }

    private String getIP(HttpServletRequest request) {
        return "212.142.124.114";
/*        String remoteAddr = request.getHeader("X-FORWARDED-FOR");
        if (StringUtils.isEmpty(remoteAddr)) {
            remoteAddr = request.getRemoteAddr();
        }
        return remoteAddr;*/
    }
}
